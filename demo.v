Load bedwyr.
Require Import Arith List. Import ListNotations.
Require Import Coq.Lists.List.

Inductive tree : Set :=
leaf : tree
| node : tree -> tree -> tree.

Inductive s1 : nat -> Prop :=
a : s1 1 | b : s1 2 | c : s1 3.
Inductive s2 : nat -> Prop :=
a1 : s2 2.

Inductive iseven: nat -> Prop :=
zz : iseven 0 | ss : forall n, iseven n -> iseven (S (S n)).
Inductive isnat: nat -> Prop :=
u : isnat 0 | m : forall n, isnat n -> isnat (S n).

Lemma qwe : forall n, s2 n -> s1 n.
intros. case n. Show Proof. exact u.

(* Elpi Trace. *)
Elpi Accumulate lp:{{
  type mkcopies list term -> list prop -> prop.
  mkcopies [] [].
  mkcopies [T|R1] [copy T U_|R] :- mkcopies R1 R.
  type mkarity term -> list term -> term -> list term -> list term -> term -> prop.
  mkarity Goal Args In BVars BVTypes Out :-
    coq.say "mkarity" Goal Args In BVars BVTypes,
    mkcopies BVars Cl,
    Cl => copy In 
  type mkbranch term -> term -> term -> list term -> list term -> term -> prop.
  mkbranch Goal K Kty BVar VTy OutTerm :-
%   (k\ kty\ lv\ lty\ t2\ check _ (bc kty {{s1 lp:X}} LT), coq.mk-app k LT t2)
    coq.say "mkbranch" K Kty BVar VTy.
}}.
(* Elpi Trace. *)
Elpi Query lp:{{
  @pi-decl _ {{nat}} n\
  @pi-decl _ {{s2 lp:n}} x\
  
  coq.safe-dest-app {{s2 lp:n}} (global (indt Prog)) (Args n),
  (coq.build-match x {{s2 lp:n}} 
  % MkRty: InputTermUnderLams LamBoundVars TheirTypes Result
  (mkarity {{s1 lp:n}} (Args n)) 
  % MkBranch: Constructor ConstructorTyUnderLams LamBoundVars TheirTypes Result
  (mkbranch _)
  (BuiltTerm x n),
  coq.say {coq.term->string (BuiltTerm x n)}).
}}.
Lemma qwe : forall n, s2 n -> s1 n.
intros. case H. Show Proof. apply b. Show Proof. Qed.
  Elpi Query lp:{{ coq.say {{(fun (n : nat) (H : s2 n) =>
 match H in (s2 n0) return (s1 n0) with
 | a1 => b
 end)}} }}.
elpi bedwyr.

intros. case H. Show Proof. apply b. Show Proof.

Elpi Query lp:{{ coq.say {{
     fun (n : nat) (H : s2 n) =>
 match H in (s2 n0) return (s1 n0) with
 | a1 => b
 end
}}}}.



Inductive reach : tree -> tree -> Prop :=
r0 : forall a: tree, reach a a
| rl : forall a b : tree, reach a (node a b) -> reach a
| rr : forall a b : tree, reach a (node b a).

Goal forall a b c d : tree, reach a (node (node b c) (node (node a leaf) leaf)).
intros. apply rr.

Goal 1 = 2 -> False. intros. inversion H. Show Proof.
Print sig. Print ex. Print existT.

Inductive natty : nat -> Prop :=
n0 : natty 0
| ns : forall n, natty n -> natty (S n).

Inductive even : nat -> Prop :=
e0 : even 0
| ess : forall n, even n -> even (S (S n)).

Inductive odd : nat -> Prop :=
o1 : odd 1 
| oss : forall n, odd n -> odd (S (S n)).

Goal exists x, x = 2 -> x = 2.
eexists. intro. auto. Show Proof. discriminate.


Goal 2 =31 -> odd 2.
Goal odd 2 -> False.
intro. Check eq_ind. discriminate. Show Proof.

Goal forall n, even n -> natty n.
intros. case H. exact n0. intros. apply (ns). apply ns.  left. exact e0. intros.   case n.

Inductive insert (x:nat) : list nat -> list nat -> Prop :=
i_n : insert x [] [x]
| i_s : forall y: nat, forall ys: list nat, x <= y -> insert x (y :: ys) (x :: y :: ys)
| i_c : forall y: nat, forall ys: list nat, forall rs: list nat,  insert x ys rs -> insert x (y :: ys) (y :: rs).


Goal
forall typ: Type,
forall tm : Type,
forall imp: typ -> typ -> typ,
forall lam: (tm -> tm) -> tm,
forall ap : tm -> tm -> tm,
forall of: tm -> typ -> Prop,

(forall X Y: tm, forall B: typ,
forall A: typ,
of X (imp A B) -> of Y B -> of (ap X Y) B)
->
(forall X: (tm -> tm), forall A: typ,
forall B: typ,
(forall t1: tm, of t1 A -> of (X t1) B) -> of (lam X) (imp A B))
->
forall t1:typ, exists T:tm, of T (imp t1 t1).
elpi bedwyr.
intros.
eexists. apply H0. intros.  exact H1. Show Proof.

Goal insert 1 [] [1].
elpi bedwyr.
Qed.
Lemma i1:  exists R, insert 2 [0;1] R.
elpi bedwyr.
Qed.
Lemma i2:  exists R, insert 2 [0;1] R.
eexists.
apply i_c.
apply i_c.
apply i_n.
Qed.
Print i2.
Print le.
 Inductive ordered : list nat -> Prop :=
onl : ordered []
| oss : forall x : nat, ordered [x]
| ocn : forall (x y : nat) (l : list nat),
     ordered (y :: l) -> x <= y -> ordered (x :: y :: l).

Goal exists Xs, ordered Xs.
eexists.
elpi prolog  3.
apply onl.
Restart.
eexists.
elpi dprolog 10.
Qed.

 Goal ordered [0;1;2;6].
   elpi prolog  10.
   Fail elpi prolog size 10.

   elpi dprolog 20.
   Qed.

   Inductive append : list nat -> list nat -> list nat -> Prop :=
   anl : forall xs, append [] xs xs
   |acn : forall xs ys zs x, 
        append xs ys zs -> append (x :: xs) ys (x :: zs).

  (* Goal exists L1 L2, append L1 L2 [0;1;2;6]. *)
  Goal append  [0] [1;2;6] [0;1;2;6].
  elpi dprolog 20.
Qed.

 Goal exists L, append  L [1;2;6] [0;1;2;6].
 eexists.
 elpi dprolog  20.
 Qed.