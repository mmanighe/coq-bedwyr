From elpi Require Export elpi.

Elpi Tactic case.
Elpi Accumulate lp:{{

  pred mk-abstracted-goal i:list term, i:term,  i:term, i:list term, i:list term, o:term.
  mk-abstracted-goal ToAbstract Goal  _IndSort Vars _VarsTys Out :-
    std.map2 ToAbstract Vars (t\v\r\ r = copy t v) Subst,
    % Non deterministically we abstract until we obtain a well typed term
    Subst => copy Goal Out,
    coq.say "trying" {coq.term->string Out},
    coq.typecheck Out _ ok.

  pred mk-empty-branches i:term, i:term, i:list term, i:list term, o:term.
  mk-empty-branches _K _KTy _Vars _VarsTys HOLE_.

  solve [trm T] [(goal Ctx _ GTy _ as G)] NG :- !,
    Ctx => (std.do! [
      std.assert-ok! (coq.typecheck T Ty) "input term illtyped",
      std.assert! (coq.safe-dest-app Ty (global (indt I)) Args) "the type is not inductive",
      coq.env.indt I _ ParamsNo _ _ _ _,
      std.drop ParamsNo Args Idxs,
      coq.say "Will abstract" Idxs,
      std.append Idxs [T] ToAbstract,
      coq.build-match T Ty (mk-abstracted-goal ToAbstract GTy) mk-empty-branches M,
      coq.say "Built Match" {coq.term->string M} M,
      refine M G NG,
      coq.say "New goals" NG
    ]).

  solve _ _ _ :- usage.

  usage :- coq.error "Usage: eltac.case <term>".

}}.
Elpi Typecheck.

Elpi Trace.
Tactic Notation "eltac.case" constr(T) := elpi case (T).

Inductive s1 : nat -> Prop :=
a : s1 1 | b : s1 2 | c : s1 3.
Inductive s2 : nat -> Prop :=
a1 : s2 2.

Inductive t1 : nat -> Prop :=
ta : forall n, n = 1 \/ n = 2 \/ n = 3 -> t1 n.

Inductive t2 : nat -> Prop :=
tb : forall n, n=2 -> t2 n.

Goal forall n, t2 n -> t1 n.
intros. eltac.case H.


Inductive iseven: nat -> Prop :=
zz : iseven 0 | ss : forall n, iseven n -> iseven (S (S n)).
Inductive isnat: nat -> Prop :=
u : isnat 0 | m : forall n, isnat n -> isnat (S n).


Lemma qwe : forall n, s2 n -> s1 n.
intros. eltac.case H. 
