From elpi Require Import elpi.

Elpi Tactic bedwyr.
Elpi Accumulate File "kernel.mod".

Elpi Accumulate lp:{{
  solve _ [goal _Ctx Ev Goal _Who] _OutGoals :-
    coq.say "Enter",
    coq.say "Goal:" {coq.term->string Goal},
    check _ (go Goal Term),
    Ev = Term.
  }}.
 Elpi Typecheck. 