Notation "A <- B" := (B -> A) (at level 85).
Infix "," := and (at level 84).

Section of_example.
(* L'hello world di lambda prolog, definisco STLC e sintentizzo un termine. *)
(* Dichiaro tipi e costanti *)
Variable tm : Type.
Variable ty : Type.
Variable lam: (tm -> tm) -> tm.
Variable app: tm -> tm -> tm.
Variable of : tm -> ty -> Prop.
Variable imp: ty -> ty -> ty.
Infix "=>" := imp (at level 85).
(* Implementazione *)
Hypothesis of_1 : forall T A B,
of (lam T) (A => B) <-
  forall x, of x A ->
    of (T x) B.
Hypothesis of_2 : forall T1 T2 A B,
of (app T1 T2) B <-
  of T1 (A => B),
  of T2 A.
(* Query *)
Goal exists T, forall P, of T (P => P).
eauto.
(* Per qualche motivo strano eauto non funziona. Scavando nei
suoi messaggi di debug, ho capito che arriva solo a fare questi
passi *)
eapply ex_intro; intro; apply of_1; intro; intro.
(* A questo punto deve semplicemente fare un altro backtrack, e
scegliere l'unico modo di costruire un termine di tipo P...
Ma non so perché non lo faccia. *)
apply H.
Qed.

End of_example.

(* Un esempio di programmazione logica con "Mondo chiuso":
calcoleremo il massimo di un insieme, in un modo che lambda Prolog
normalmente non sa fare. Per farlo estendo eauto chiedendogli di fare
unfold degli induttivi e risolvere le uguaglianze *)
Section inductive_example.
Hint Extern 1 (_ <> _) => congruence.
Hint Extern 1 (_ = _) => congruence.
Hint Extern 1 =>
  let H := fresh in
  match goal with
    | [_:_ |- ?A -> ?B ] => intro H; case H
  end.

Variable n : Type.
Variable z : n.
Variable s : n -> n.

Inductive a : n -> Prop :=
  a1 : a (s (s (s z)))
| a2 : a (s (s (s (s (s z)))))
| a3 : a (s (s z)).

(* Ora scrivo anche i programmi come induttivi: questo non è
necessario, potrei darli come Hypothesis, come prima. Il punto importante
è che i dati (il tipo a) sono induttivi *)
(* Leq funziona anche in lambda Prolog *)
Inductive leq : n -> n -> Prop :=
  le1 : forall N, leq z N
| le2 : forall N M, leq (s N) (s M) <- leq N M.
(* Questo max, invece, non funziona in lambda Prolog: la semantica standard
dell'implicazione nel corpo della definizione direbbe di assumere un elemento
fresh, e poi vedere se è più piccolo; questo fallisce sempre, dato che l'elemento
fresh non è confrontabile.
Invece ora abbiamo la nuova semantica che decostruisce i tipi induttivi, quindi max
ha successo sui tipi induttivi*)
Inductive max : (n -> Prop) -> n -> Prop :=
  mm : forall A N, max A N <-
    A N,
    forall x, A x -> leq x N.
Hint Constructors max.
Hint Constructors leq.
Hint Constructors a.
Goal exists X, max a X.
eauto 20.
Qed.
Goal exists X, (max a X /\ X = (s (s (s (s (s z)))))).
eauto 20.
Qed.

End inductive_example.