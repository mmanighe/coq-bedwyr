module kernel.

% check A B :- coq.say "check" A B, fail.
check Cert (bc A1 A2 Terms) :-
  coq.say "bc",
  coq.term->string A1 S1,
  coq.term->string A2 S2,
  coq.say S1 S2,
  std.map Terms (x\y\ coq.term->string x y) T1,
  coq.say "check bc" S1 S2 T1, fail.
check Cert (go A Term ):-
  coq.term->string A S,
  coq.term->string Term T1,
  coq.say "check go" S T1, fail.

check Cert (go X Term ):-
  var X,
  declare_constraint (check Cert (go X Term)) [X].

check _ (go (sort S) Term ):-
%  coq.say "Term" Term "Sort" S,
  coq.typecheck Term (sort S) _. %% Resort to Coq typechecking: we could do better

% check Cert (go {{lp:G = lp:G}} {{eq_refl}}).
	% eq_expert Cert.

check Cert (go (prod _ Ty1 Ty2) (fun _ Ty1 (x\ match x Rty Bs))) :-
	pi x\ decl x _ Ty1 => 
  coq.say "Hi",
  coq.safe-dest-app Ty1 (global (indt GR)) Args,
  coq.env.indt GR _ ParamsNo _ Arity Kn Kt,
  std.drop ParamsNo Args Idxs,
  std.take ParamsNo Args LArgs,
  coq.mk-app (global (indt GR)) LArgs IndtLArgs,
  coq.subst-prod LArgs Arity ArityArgs,
  coq.say "Alive",
  coq.bind-ind-arity IndtLArgs ArityArgs (mk-abstracted-goal ToAbstract (Ty2 _)) Rty,
  coq.say "Arity" Rty,
  std.map Kt (coq.subst-prod LArgs) KtArgs,
  std.map KtArgs coq.prod->fun KtArgsLam,
  std.map Kn (k\ coq.mk-app (global (indc k)) LArgs) KnArgs,
  % std.map2 KnArgs KtArgsLam (k\t\coq.map-under-fun t (mk-empty-branches Rty k)) Bs.
  std.map2 KtArgsLam Kn (mk-bty Rty ParamsNo) BsTy,
  coq.say "BsTy" BsTy,
  std.map2 BsTy Kt (bsty\ kty\ l\ check _ (bc kty bsty l)) BsArgs,
  std.map2 Kn BsArgs (k\ l\ coq.mk-app (global (indc k)) l) Bs.

check Cert (go Atom Term) :-
  coq.safe-dest-app Atom (global (indt Prog)) _Args,
  coq.env.indt Prog _ _ _ _Type Kn Clauses,
  std.mem Kn K,
	Kons = global (indc K),
	% unfold_expert Kn Cert Cert' K,
	%% Use the selected constructor as key to find its
	%% clause in the zipped list of constructors and clauses.
	std.lookup {std.zip Kn Clauses} K Clause, 
	check Cert' (bc Clause Atom ListArgs),
  coq.mk-app Kons ListArgs Term.

%% Perform simple reduction in the head
check Cert (go (app [(fun A B C)| Args]) Term) :-
  coq.mk-app (fun A B C) Args App,
  check Cert (go App Term).

%% backchain

check Cert (bc A A' []) :-
	% tt_expert Cert,
  coq.unify-eq A A' ok,
  coq.say "Ok" A A'.
	
check Cert (bc (prod _ Ty1 Ty2) Goal OutTerm) :-
  % prod_expert Cert Cert1 Cert2,
  check Cert1 (bc (Ty2 Tm) Goal ListArgs),
  check Cert2 (go Ty1 Tm),
  OutTerm = [Tm|ListArgs].

%% Decide and unfold-left
check Cert (go Goal OutTerm) :-
  store Var LAtom,
  coq.say "Retrieved" LAtom,% {coq.term->string LAtom},
  coq.safe-dest-app LAtom (global (indt Prog)) Args,
  % coq.env.indt Prog _ _ _ _Type Kn Clauses,

  % coq.typecheck T Ty,
  % coq.safe-dest-app Ty (global (indt I)) Args),
  coq.env.indt Prog _ ParamsNo _ _ Kn Clauses,
  std.drop ParamsNo Args Idxs,
  std.append Idxs [Var] ToAbstract,
  buildmatch Var LAtom Goal ToAbstract (match Var Fun LL),
  OutTerm = (match Var Fun LL).

pred mk-abstracted-goal i:list term, i:term, i:term, i:list term, i:list term, o:term.
mk-abstracted-goal ToAbstract Goal _IndSort Vars _VarsTys Out :-
  std.map2 ToAbstract Vars (t\v\r\ r = copy t v) Subst,
  % Non deterministically we abstract until we obtain a well typed term
  Subst => copy Goal Out,
  coq.say "trying" {coq.term->string Out},
  coq.typecheck Out _ ok,
  coq.say "Ok mkabsgoal:" Out.

pred mk-empty-branches i:term, i:term, i:term, i:list term, i:list term, o:term.
mk-empty-branches Goal K KTy _Vars _VarsTys OutTerm_ :-
  % get_head Goal GoalHead,
  % check _ (bc KTy GoalHead L),
  % coq.mk-app K L OutTerm,
  coq.say "Ok mkbranch".

pred get_head i:term, o:term.
get_head (fun _ _ B) C :- !, get_head (B X_) C.
get_head A A. 

pred buildmatch
  i:term, % T, the term being matched
  i:term, % the type of T, expected to be an inductive, eventually applied
  i:term, % Goal
  i:list term, % ToAbstract
  o:term. % match T (.. MkRty) [ .. MkBranch K1, .. MkBranch K2, ..]
buildmatch T Tty Goal ToAbstract (match T Rty Bs) :-
  whd Tty [] (global (indt GR)) Args,
  coq.env.indt GR _ Lno _ Arity Kn Kt,
  std.take Lno Args LArgs,
  coq.mk-app (global (indt GR)) LArgs IndtLArgs,
  coq.say "D",
  % Rty
  coq.subst-prod LArgs Arity ArityArgs,
  coq.say "Try binding",
  coq.bind-ind-arity IndtLArgs ArityArgs (mk-abstracted-goal ToAbstract Goal) Rty,
  coq.say "Rty" Rty,
  % Bs
  std.map Kt (coq.subst-prod LArgs) KtArgs,
  std.map KtArgs coq.prod->fun KtArgsLam,
  std.map Kn (k\ coq.mk-app (global (indc k)) LArgs) KnArgs,
  % std.map2 KnArgs KtArgsLam (k\t\coq.map-under-fun t (mk-empty-branches Rty k)) Bs.
  std.map2 KtArgsLam Kn (mk-bty Rty Lno) BsTy,
  coq.say "BsTy" BsTy,
  std.map2 BsTy Kt (bsty\ kty\ l\ check _ (bc kty bsty l)) BsArgs,
  std.map2 Kn BsArgs (k\ l\ coq.mk-app (global (indc k)) l) Bs.

type mk-bty term -> int -> term -> constructor -> term -> prop.
mk-bty Rty Lno (prod N S T) Ki (prod N S B) :- !,
  pi x\ mk-bty Rty Lno (T x) Ki (B x).
mk-bty Rty Lno T Ki AppRty :-
  coq.safe-dest-app T (global (indt _)) Args,
  std.split-at Lno Args LArgs RArgs,
  coq.mk-app Rty {std.append RArgs [{coq.mk-app (global (indc Ki)) {std.append LArgs RArgs}}]} AppRty.